# -*- coding: utf-8 -*-
from app import app
from app.forms import LoginForm

from flask import render_template, redirect, flash


@app.route('/')
@app.route('/index')
def idex():
    user = {"nickname": "Topa"}
    posts =  [
        {"author": {"nickname": "helga"}, "title": 'Title 1',
        "body": "Был скандал, Влада не делает укр язык ",
        "timestamp": "01.02.2022"},
         {"author": {"nickname": "helga"}, "title": 'Title 2',
        "body": "Был скандал, Влада не делает укр язык ",
        "timestamp": "02.02.2022"},
          {"author": {"nickname": "helga"}, "title": 'Title 23',
        "body": "Был скандал, Влада не делает укр язык ",
        "timestamp": "11.02.2022"}]
    return render_template("index.html", title="Home", user=user, posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash(f'Login requested for {form.username.data}, remember_me = {form.remember_me.data}')
        return redirect('/index')
    return render_template('login.html', title='Sing In', form=form)
