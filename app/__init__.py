from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

import config


app = Flask(__name__)
if app.config['ENV'] == "production":
    app.config.from_object(config.ProductionConfig)
elif app.config['ENV'] == "testing":
    app.config.from_object(config.TestingConfig)
else:
    app.config.from_object(config.DevelopmentConfig)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

print(f'ENV is set to: {app.config["ENV"]}')

from app import routes, models
